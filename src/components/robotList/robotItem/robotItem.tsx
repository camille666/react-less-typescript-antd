import * as React from 'react'
import styles from './robotItem.less'
export interface IRobotItemProps {
    handleClick: any
    mediaName: string
    mediaOverview: string
    icon: string
    used: boolean
    valid: boolean
}

export default class RobotItem extends React.Component<IRobotItemProps> {
    constructor(props: IRobotItemProps) {
        super(props)
    }
    public render() {
        const { mediaName, mediaOverview, icon, used, valid } = this.props
        let btnText = <div />
        let jbText = <div />
        if (used === false) {
            btnText = (
                <a className={styles.btn} onClick={this.props.handleClick}>
                    立即接入20天
                </a>
            )
            jbText = <div />
        } else if (used === true && valid === false) {
            btnText = (
                <a className={styles.btn} onClick={this.props.handleClick}>
                    申请延期
                </a>
            )
            jbText = <div className={styles.ydq}>已到期</div>
        } else if (used === true && valid === true) {
            btnText = (
                <a className={styles.btn} onClick={this.props.handleClick}>
                    去设置
                </a>
            )
            jbText = <div />
        } else {
            btnText = <div />
            jbText = <div />
        }
        return (
            <div className={styles.box}>
                <div className={styles.left}>
                    <img src={icon} />
                    {jbText}
                </div>
                <div className={styles.right}>
                    <div className={styles.title}>{mediaName}</div>
                    <div className={styles.desc}>{mediaOverview}</div>
                    {btnText}
                </div>
            </div>
        )
    }
}
