import axios from 'axios'
import * as React from 'react'
import { NavLink } from 'react-router-dom'
import styles from './menu.less'

interface IMenu {
    id: number
    name: string
    children?: IMenuSub[]
}

interface IMenuSub {
    name: string
    id: number
}

export default class StoreMenu extends React.Component {
    public state = {
        menuList: []
    }

    public componentDidMount() {
        const that = this

        axios.get('/api/menu').then(function(response) {
            that.setState({
                menuList: response.data.data.menus
            })
        })
    }

    public menuIsActive = (item: any, menuActiveName: any) => {
        let cls = ''
        // 处理一级页面，菜单高亮
        if (location.pathname.includes(`menu${item.id}-sub`)) {
            cls = styles.selectedItem
        }

        // 处理二级页面，菜单高亮，两个详情页，一个列表页
        // 机器人列表页
        if (
            item.id === 3 &&
            location.pathname.includes('robot') &&
            menuActiveName !== '精选应用'
        ) {
            cls = styles.selectedItem
        }

        // 详情页
        if (
            item.id === 2 &&
            location.pathname.includes('detail') &&
            menuActiveName !== '精选应用'
        ) {
            cls = styles.selectedItem
        }

        return cls
    }

    // 高亮营销作战室菜单
    public activeMiddle = (
        menuActiveName: any,
        item: IMenu,
        subitem: IMenuSub,
        location: any
    ) => {
        return item.id === 2 &&
            location.pathname.includes('detail') &&
            subitem.name === menuActiveName
            ? true
            : false
    }

    // 高亮数据菜单
    public activeLast = (menuActiveName: any, item: IMenu, location: any) => {
        return item.id === 3 &&
            location.pathname.includes('robot') &&
            menuActiveName === '数据接入'
            ? true
            : false
    }

    // render菜单项
    public renderMenu = (item: IMenu) => {
        const menuActiveName = localStorage.getItem('curpage')
        if (!item.children) {
            return (
                <div className={styles.eachMenu} key={item.id}>
                    <div className={styles.title}>
                        <NavLink
                            to={`/appstore/menu${item.id}`}
                            activeClassName={styles.selectedItem}
                            isActive={(match: any, location: any) =>
                                match ||
                                (menuActiveName === '精选应用' &&
                                    !location.pathname.includes('menu3') &&
                                    !location.pathname.includes('menu2'))
                                    ? true
                                    : false
                            }
                        >
                            <i className="iconfont icon-jingxuanyingyong" />
                            <span>{item.name}</span>
                        </NavLink>
                    </div>
                </div>
            )
        } else if (!!item.children && !!item.children.length) {
            const iconName = item.name.includes('营销')
                ? 'icon-yingxiaozuozhanshi'
                : 'icon-shujujiqiren'

            return (
                <div className={styles.eachMenu} key={item.id}>
                    <div className={styles.title}>
                        <a className={this.menuIsActive(item, menuActiveName)}>
                            <i className={`iconfont ${iconName}`} />
                            <span>{item.name}</span>
                        </a>
                    </div>
                    <ul className={styles.subList}>
                        {item.children.map((subitem: IMenuSub) => {
                            return (
                                <li key={'sub' + subitem.id}>
                                    <NavLink
                                        to={`/appstore/menu${item.id}-sub${subitem.id}`}
                                        activeClassName={styles.selectedItem}
                                        isActive={(match: any, location: any) =>
                                            match ||
                                            this.activeLast(
                                                menuActiveName,
                                                item,
                                                location
                                            ) ||
                                            this.activeMiddle(
                                                menuActiveName,
                                                item,
                                                subitem,
                                                location
                                            )
                                        }
                                    >
                                        {subitem.name}
                                    </NavLink>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            )
        } else {
            return <div />
        }
    }

    public render() {
        const menuData = this.state.menuList
        return (
            <div className={styles.menuBox}>
                {menuData.map(item => {
                    return this.renderMenu(item)
                })}
            </div>
        )
    }
}
