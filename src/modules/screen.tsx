import { Button, Checkbox, message, Modal } from 'antd'
import axios from 'axios'
import * as React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import SwitchBig from '../components/switchBig/switchBig'
import styles from './screen.less'

interface IWindowInfo {
    appTrialDesc?: string
}

interface IIntroItem {
    imageUrl: string
    intro: string
}

interface IDetail {
    intro: IIntroItem[]
    appUrl: string
    appType: string
    level: number
    appTitle?: string
    appName?: string
    brief_intro?: string
    userAppStatus?: number
}

interface IYxScreenState {
    lId: string
    checked: boolean
    checkedYq: boolean
    visible: boolean
    yVisible: boolean
    appDetail: IDetail
    originTitle?: string
    windowInfo?: IWindowInfo
}

export default class YxScreen extends React.Component<
    RouteComponentProps,
    IYxScreenState
> {
    constructor(props: RouteComponentProps) {
        super(props)
        this.state = {
            lId: this.props.history.location.pathname.split('detail1/')[1],
            checked: false,
            checkedYq: false,
            visible: false,
            yVisible: false,
            originTitle: '',
            appDetail: {
                intro: [],
                appName: '',
                appType: '',
                level: 1,
                appUrl: '',
                appTitle: '',
                brief_intro: '',
                userAppStatus: 0
            },
            windowInfo: {}
        }
    }

    public showModal = () => {
        this.setState({
            visible: true
        })
    }

    public handleOk = (e: any) => {
        this.setState({
            visible: false
        })
        this.applyTime()
    }

    public handleCancel = (e: any) => {
        this.setState({
            visible: false,
            checked: false
        })
    }

    public showModalYq = () => {
        this.setState({
            yVisible: true
        })
    }

    public handleOkYq = (e: any) => {
        this.setState({
            yVisible: false
        })
        this.applyTimeYq()
    }

    public handleCancelYq = (e: any) => {
        this.setState({
            yVisible: false,
            checkedYq: false
        })
    }

    public handleChangeCkb = (e: any) => {
        this.setState({
            checked: e.target.checked
        })
    }

    public handleChangeCkbYq = (e: any) => {
        this.setState({
            checkedYq: e.target.checked
        })
    }

    // 试用或申请延期
    public applyTime = () => {
        const that = this
        axios.post(`/api/tryUse`).then(function(response: any) {
            // 重新请求页面数据
            that.getDetail()
            message.config({
                top: window.screen.height / 2
            })
            message.success('开通成功！已添加到首页')
        })
    }

    public applyTimeYq = () => {
        const that = this
        axios.post(`/api/tryUse`).then(function(response: any) {
            // 重新请求页面数据
            that.getDetail()
            message.config({
                top: window.screen.height / 2
            })
            message.success('申请成功！已添加到首页')
        })
    }

    // 获取本页面数据
    public getDetail = () => {
        const that = this

        axios.get(`/api/detailA`).then(function(response: any) {
            that.setState({
                appDetail: response.data.data
            })
        })
    }

    // 获取弹窗信息
    public getWindowInfo = () => {
        const that = this
        axios.get(`/api/popWinA`).then(function(response: any) {
            that.setState({
                windowInfo: response.data.data
            })
        })
    }

    public componentDidMount() {
        this.setState({
            lId: this.props.history.location.pathname.split('detail1/')[1],
            originTitle: window.localStorage.getItem('curpage')!
        })
        this.getDetail()
        this.getWindowInfo()
    }

    public componentDidUpdate(prevProps: any, prevState: IYxScreenState) {
        if (prevState.appDetail.appTitle !== this.state.appDetail.appTitle) {
            this.getDetail()
        }
    }

    public render() {
        const {
            appName,
            brief_intro,
            intro,
            userAppStatus,
            appUrl
        } = this.state.appDetail!

        const { appTrialDesc } = this.state.windowInfo!

        let btnText = <div />
        let jb = <div />

        if (userAppStatus === 0) {
            btnText = (
                <a className={styles.btn} onClick={this.showModal}>
                    立即试用20天
                </a>
            )
        } else if (userAppStatus === 1 || userAppStatus === 2) {
            btnText = (
                <a className={styles.btn} href={appUrl}>
                    去使用
                </a>
            )
        } else if (userAppStatus === 3) {
            btnText = (
                <a className={styles.btn} onClick={this.showModalYq}>
                    申请延期
                </a>
            )
            jb = <span>已到期</span>
        }

        return (
            <div className={styles.screenContent}>
                <div className={styles.bread}>
                    <a onClick={() => this.props.history.go(-1)}>
                        {this.state.originTitle}
                    </a>
                    <span
                        className={`iconfont icon-mianbaoxie ${styles.arrowS}`}
                    />
                    <span className={styles.cur}>{appName}</span>
                </div>
                <div className={styles.detailBlock}>
                    <div className={styles.left}>
                        <div className={styles.bigIcon}>
                            {userAppStatus === 3 ? jb : ''}
                            <img src={''} />
                        </div>
                        {btnText}
                    </div>
                    <div className={styles.right}>
                        <div className={styles.title}>{appName}</div>
                        <div className={styles.details}>{brief_intro}</div>
                    </div>
                </div>
                <SwitchBig data={intro} />
                <Modal
                    title="试用说明"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="暂不开通" onClick={this.handleCancel}>
                            暂不开通
                        </Button>,
                        <Button
                            key="开通试用"
                            type="primary"
                            onClick={this.handleOk}
                            disabled={this.state.checked ? false : true}
                        >
                            开通试用
                        </Button>
                    ]}
                >
                    {appTrialDesc &&
                        appTrialDesc.split('\n').map((item, index) => {
                            return (
                                <p
                                    key={index}
                                    style={
                                        index === 1 ||
                                        index ===
                                            appTrialDesc.split('\n').length - 1
                                            ? { textIndent: '28px' }
                                            : {}
                                    }
                                >
                                    {item}
                                </p>
                            )
                        })}
                    <Checkbox
                        style={{
                            marginTop: '10px',
                            color: 'rgba(0,126,240,1)'
                        }}
                        onChange={this.handleChangeCkb}
                        checked={this.state.checked ? true : false}
                    >
                        同意
                    </Checkbox>
                </Modal>

                <Modal
                    title="申请延期"
                    visible={this.state.yVisible}
                    onOk={this.handleOkYq}
                    onCancel={this.handleCancelYq}
                    footer={[
                        <Button key="暂不申请" onClick={this.handleCancelYq}>
                            暂不申请
                        </Button>,
                        <Button
                            key="申请延期"
                            type="primary"
                            onClick={this.handleOkYq}
                            disabled={this.state.checkedYq ? false : true}
                        >
                            申请延期
                        </Button>
                    ]}
                >
                    {appTrialDesc &&
                        appTrialDesc.split('\n').map((item, index) => {
                            return (
                                <p
                                    key={index}
                                    style={
                                        index === 1 ||
                                        index ===
                                            appTrialDesc.split('\n').length - 1
                                            ? { textIndent: '28px' }
                                            : {}
                                    }
                                >
                                    {item}
                                </p>
                            )
                        })}
                    <Checkbox
                        style={{
                            marginTop: '10px',
                            color: 'rgba(0,126,240,1)'
                        }}
                        onChange={this.handleChangeCkbYq}
                        checked={this.state.checkedYq ? true : false}
                    >
                        同意
                    </Checkbox>
                </Modal>
            </div>
        )
    }
}
