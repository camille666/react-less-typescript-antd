module.exports = {
    code: 0,
    data: {
        apps: [
            {
                appId: 'app1',
                appIcon: '',
                level: 2,
                appTitle: '',
                appName: '数据机器人可以点击',
                appType: 'data',
                brief_intro: '',
                keywords: '专业服务 精准定位 品牌营销',
                overview: '帮助企业多渠道接入数据',
                appStatus: 1
            },
            {
                appId: 'app2',
                appIcon: '',
                level: 1,
                appTitle: 'create',
                appName: '非数据和营销可以点击',
                appType: 'battle',
                brief_intro: '',
                keywords: '实时创意 历史创意 优质创意',
                overview: '帮助您评估近期广告创意效果',
                appStatus: 1
            },
            {
                appId: 'app3',
                appIcon: '',
                level: 1,
                appTitle: 'zuan',
                appName: '非数据和营销可以点击',
                appType: 'url',
                brief_intro: '',
                keywords: '数据分析 实时数据 报表下载',
                overview: '智能优化钻石展位',
                appStatus: 1
            },
            {
                appId: 'app4',
                appIcon: '',
                level: 1,
                appTitle: 'expert',
                appName: '营销可以点击',
                appType: 'service',
                brief_intro: '营销服务营销服务营销服务营销服务营销服务',
                keywords: '实时数据 历史数据 渠道数据',
                overview: '展示钻展，优化投放',
                appStatus: 1
            }
        ]
    },
    message: ''
}
