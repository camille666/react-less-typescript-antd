const paths = require('./paths.js')
const path = require('path')
const ROOT_PATH = paths.ROOT_PATH
const OUTPUT_PATH = paths.OUTPUT_PATH
const SOURCE_PATH = paths.SOURCE_PATH
const OUTPUT_ASSETS_PATH = paths.OUTPUT_ASSETS_PATH

const CopyWebpackPlugin = require('copy-webpack-plugin')

// 在编译之前，把旧的编译文件清空，否则会持续生成不同hash的文件，我们只需要保留最新编译的文件。
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
// 主题插件

// 导入antd，按需加载
const tsImportPluginFactory = require('ts-import-plugin')
// 让tsconfig的paths配置生效
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')

// 入口文件配置
const ENTRIES_CONFIG = {
    main: SOURCE_PATH + '/app.tsx'
}

// 输出配置
const OUTPUT_CONFIG = {
    publicPath: '/',
    path: OUTPUT_PATH,
    filename: '[name].bundle.js',
    chunkFilename: '[name].[chunkHash:5].chunk.js'
}

// 处理的文件类型和范围
const RESOLVE_CONFIG = {
    extensions: ['.ts', '.tsx', '.js'],
    plugins: [
        new TsconfigPathsPlugin({
            configFile: path.resolve(ROOT_PATH, './tsconfig.json'),
            logLevel: 'info',
            extensions: ['.ts', '.tsx']
        })
    ]
}

// 备份资源插件
let copyWebpackPlugin = new CopyWebpackPlugin([
    // 复制所有当前工程的 assets 到 output 目录
    {
        context: SOURCE_PATH,
        from: 'assets/',
        to: OUTPUT_ASSETS_PATH + '/'
    }
])

// 插件
const PLUGINS = [new CleanWebpackPlugin(), copyWebpackPlugin]

// 加载器
const MODULE_RULES = [
    {
        test: /\.tsx?$/,
        use: [
            {
                loader: 'ts-loader',
                options: {
                    transpileOnly: true,
                    getCustomTransformers: () => ({
                        before: [
                            tsImportPluginFactory({
                                libraryName: 'antd',
                                libraryDirectory: 'lib',
                                style: true
                            })
                        ]
                    })
                }
            }
        ],
        exclude: /node_modules/
    },
    {
        test: /\.(ttf|eot|svg|png|gif|jpg|jpeg|swf)$/,
        exclude: /node_modules/,
        include: /assets/,
        loader: 'file-loader'
    }
]

module.exports = {
    entry: ENTRIES_CONFIG,
    output: OUTPUT_CONFIG,
    resolve: RESOLVE_CONFIG,
    plugins: PLUGINS,
    module: { rules: MODULE_RULES }
}
