import { Icon } from 'antd'
import * as React from 'react'
import styles from './transformBig.less'

interface ITransformBigState {
    currentIndex: number
}

interface IIntroItem {
    imageUrl: string
    intro: string
}

interface ITransformBigProps {
    data: IIntroItem[]
}

// 小图切换大图
export default class TransformBig extends React.Component<
    ITransformBigProps,
    ITransformBigState
> {
    constructor(props: ITransformBigProps) {
        super(props)
        this.state = {
            currentIndex: 0
        }
    }

    /**
     * 点击左右箭头
     * 1、当前箭头是否激活，改变箭头样式；
     * 2、切换大图
     */
    public handlePrevNext = (type: string) => {
        let newCurrent = this.state.currentIndex
        if (type === 'prev') {
            newCurrent--
            this.setState({
                currentIndex:
                    newCurrent < 0
                        ? this.props.data.concat(this.props.data).reverse()
                              .length - 1
                        : newCurrent
            })
        } else if (type === 'next') {
            newCurrent++
            this.setState({
                currentIndex:
                    newCurrent ===
                    this.props.data.concat(this.props.data).reverse().length
                        ? 0
                        : newCurrent
            })
        }
    }

    public renderImgInfoList = () => {
        let imgStyle = ''
        const imgDomArr = []
        const textDomArr = []
        for (
            let i = this.state.currentIndex - 1;
            i < this.state.currentIndex + 2;
            i++
        ) {
            const imgIndex =
                (i + 1) %
                this.props.data.concat(this.props.data).reverse().length
            switch (this.state.currentIndex - i) {
                case -1:
                    imgStyle = styles.prevImg

                    break
                case 0:
                    imgStyle = styles.currentImg

                    break
                case 1:
                    imgStyle = styles.nextImg

                    break
                default:
                    imgStyle = styles.currentImg
                    break
            }

            // 折叠图
            const eachData = this.props.data.concat(this.props.data).reverse()[
                imgIndex
            ]
            if (eachData) {
                imgDomArr.push(
                    <div
                        key={imgIndex}
                        className={styles.img + ' ' + imgStyle}
                        style={{
                            backgroundImage: `url(${
                                this.props.data
                                    .concat(this.props.data)
                                    .reverse()[imgIndex].imageUrl
                            })`,
                            backgroundSize: 'contain',
                            backgroundRepeat: 'no-repeat',
                            backgroundPosition: 'center center'
                        }}
                    />
                )
            }

            // 联动文本
            if (eachData) {
                textDomArr.push(
                    <p>
                        {
                            this.props.data.concat(this.props.data).reverse()[
                                imgIndex
                            ].intro
                        }
                    </p>
                )
            }
        }

        return {
            imgList: imgDomArr.reverse(),
            textCurrent: textDomArr.reverse()[1]
        }
    }

    public render() {
        return (
            <div className={styles.transBox}>
                <div className={styles.cpjs}>产品介绍:</div>
                <div className={styles.descBox}>
                    {this.renderImgInfoList().textCurrent}
                </div>
                <div className={styles.bigBox}>
                    <a
                        onClick={this.handlePrevNext.bind(this, 'prev')}
                        className={styles.goLeftBtn}
                    >
                        <Icon type="double-left" style={{ fontSize: '15px' }} />
                    </a>
                    <div className={styles.bigPic}>
                        {this.renderImgInfoList().imgList.map((item, index) => {
                            return item
                        })}
                    </div>
                    <a
                        onClick={this.handlePrevNext.bind(this, 'next')}
                        className={styles.goRightBtn}
                    >
                        <Icon
                            type="double-right"
                            style={{ fontSize: '15px' }}
                        />
                    </a>
                </div>
            </div>
        )
    }
}
