import * as React from 'react'
import { Route, Switch } from 'react-router-dom'
import { RouteComponentProps } from 'react-router-dom'
import styles from './index.less'

const StoreHeader = React.lazy(() =>
    import('components/storeHeader/storeHeader')
)

const StoreMenu = React.lazy(() => import('components/menu/menu'))

const AppList = React.lazy(() => import('modules/appList'))

const AiList = React.lazy(() => import('modules/aiList'))

const YyList = React.lazy(() => import('modules/yyList'))

const SjdpList = React.lazy(() => import('modules/sjdpList'))

const YxzjList = React.lazy(() => import('modules/yxzjList'))

const SjjrList = React.lazy(() => import('modules/sjjrList'))

const Robot = React.lazy(() => import('modules/robot'))

const YxScreen = React.lazy(() => import('modules/screen'))

const YxService = React.lazy(() => import('modules/service'))

export default class StoreHome extends React.Component<RouteComponentProps> {
    constructor(props: RouteComponentProps) {
        super(props)
    }

    public render() {
        return (
            <div className={styles.pageBox}>
                <StoreHeader />
                <div className={styles.contentBox}>
                    <StoreMenu />

                    <Switch>
                        <Route
                            exact={true}
                            path="/appstore/menu1"
                            component={AppList}
                        />
                        <Route
                            exact={true}
                            path="/appstore/menu2-sub4"
                            component={AiList}
                        />
                        <Route
                            exact={true}
                            path="/appstore/menu2-sub5"
                            component={YyList}
                        />
                        <Route
                            exact={true}
                            path="/appstore/menu2-sub6"
                            component={SjdpList}
                        />
                        <Route
                            exact={true}
                            path="/appstore/menu2-sub7"
                            component={YxzjList}
                        />
                        <Route
                            exact={true}
                            path="/appstore/menu3-sub8"
                            component={SjjrList}
                        />
                        <Route
                            exact={true}
                            path="/appstore/detail1/:id"
                            component={YxScreen}
                        />
                        <Route
                            exact={true}
                            path="/appstore/detail2/:id"
                            component={YxService}
                        />
                        <Route
                            exact={true}
                            path="/appstore/robot/:id"
                            component={Robot}
                        />
                    </Switch>
                </div>
            </div>
        )
    }
}
