module.exports = {
    code: 0,
    data: {
        menus:[{
                id: 1,
                level: 1,
                name: '精选应用',
                iconCode:'',
                children: null
            },
            {
                id:2,
                level:1,
                name:'营销作战室',
                iconCode:'',
                children: [{
                    id:4,
                    level:2,
                    name:'AI机器人',
                    iconCode:'',
                    children: null
                },
                {
                    id:5,
                    level:2,
                    name:'运营作战室',
                    iconCode:'',
                    children: null
                },
                {
                    id:6,
                    level:2,
                    name:'数据大屏',
                    iconCode:'',
                    children: null
                },
                {
                    id:7,
                    level:2,
                    name:'营销专家服务',
                    iconCode:'',
                    children: null
                }]
            },
            {
                id:3,
                level:1,
                name:'数据机器人',
                iconCode:'',
                children: [{
                    id:8,
                    level:2,
                    name:'数据接入',
                    iconCode:'',
                    children: null
                }]
            }
        ]
    },
    message: ''
}
