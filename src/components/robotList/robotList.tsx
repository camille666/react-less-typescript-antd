import { Icon } from 'antd'
import { Button, Checkbox, Modal } from 'antd'
import axios from 'axios'
import * as React from 'react'
import RobotItem from './robotItem/robotItem'
import styles from './robotList.less'

interface IRitem {
    mediaId: string
    mediaName: string
    valid: boolean
    used: boolean
    mediaTitle: string
    mediaUrl: string
    mediaDescribe: string
    mediaOverview: string
    icon: string
}

interface IRobotState {
    mediaId: string
    checked: boolean
    checkedYq: boolean
    visible: boolean
    yVisible: boolean
    cVisible: boolean
    activeItem: IRitem
    rList: IRitem[]
}
export default class RobotList extends React.Component<{}, IRobotState> {
    constructor(props: {}) {
        super(props)
        this.state = {
            checked: false,
            checkedYq: false,
            visible: false,
            yVisible: false,
            cVisible: false,
            mediaId: '',
            activeItem: {
                icon: '',
                mediaId: '',
                mediaName: '',
                valid: false,
                used: false,
                mediaTitle: '',
                mediaUrl: '',
                mediaDescribe: '',
                mediaOverview: ''
            },
            rList: []
        }
    }
    public showModal = (item: IRitem) => {
        this.setState({
            checked: false,
            checkedYq: false,
            visible: true,
            activeItem: item
        })
    }

    public handleOk = (e: any) => {
        this.setState({
            visible: false
        })
        this.applyTime()
    }

    public handleCancel = (e: any) => {
        this.setState({
            visible: false
        })
    }

    public showModalYq = (item: IRitem) => {
        this.setState({
            checked: false,
            checkedYq: false,
            yVisible: true,
            activeItem: item
        })
    }

    public handleOkYq = (e: any) => {
        this.setState({
            yVisible: false
        })
        this.applyTimeYq()
    }

    public handleCancelYq = (e: any) => {
        this.setState({
            yVisible: false
        })
    }

    public handleChangeCkb = (e: any) => {
        this.setState({
            checked: e.target.checked
        })
    }

    public handleChangeCkbYq = (e: any) => {
        this.setState({
            checkedYq: e.target.checked
        })
    }

    // 试用
    public applyTime = () => {
        const that = this

        const req = {
            mediaId: this.state.activeItem.mediaId,
            setType: 'delay'
        }

        axios.patch(`/api/rList`, req).then(function(response: any) {
            if (!response.data.code) {
                that.showSuccessModal()
            }
        })
    }

    // 延期，patch请求
    public applyTimeYq = () => {
        const that = this

        const req = {
            mediaId: this.state.activeItem.mediaId,
            setType: 'delay'
        }

        axios.patch(`/api/rList`, req).then(function(response: any) {
            if (!response.data.code) {
                that.showSuccessModalYq()
            }
        })
    }

    // 获取本页面数据
    public getDetail = () => {
        const that = this
        axios.get(`/api/rList`).then(function(response: any) {
            that.setState({
                rList: response.data.data
            })
        })
    }

    public componentDidMount() {
        this.getDetail()
    }

    public keepHere = () => {
        this.setState({
            cVisible: false
        })
        this.getDetail()
    }

    public goToSet = () => {
        this.setState({
            cVisible: false
        })
        window.location.href = this.state.activeItem.mediaUrl
    }

    // 显示延期成功
    public showSuccessModal = () => {
        Modal.confirm({
            title: '申请成功！',
            okText: '去设置>>',
            icon: <Icon type="check-circle" style={{ color: '#52C419' }} />,
            cancelText: '留在此页',
            onOk: this.goToSet,
            onCancel: this.keepHere
        })
    }

    public showSuccessModalYq = () => {
        Modal.confirm({
            title: '延期成功！',
            okText: '去设置>>',
            icon: <Icon type="check-circle" style={{ color: '#52C419' }} />,
            cancelText: '留在此页',
            onOk: this.goToSet,
            onCancel: this.keepHere
        })
    }

    public btnClickEvent = (item: IRitem) => {
        const { valid, used } = item
        if (valid === true && used === true) {
            // 去设置
            window.location.href = item.mediaUrl
        } else if (valid === false && used === true) {
            this.showModalYq(item)
        } else {
            this.showModal(item)
        }
    }

    public render() {
        const appTrialDesc = this.state.activeItem.mediaDescribe

        return (
            <div className={styles.listBlock}>
                <div className={styles.listBox}>
                    {this.state.rList.map((item: IRitem, index: number) => {
                        return (
                            <RobotItem
                                key={index}
                                handleClick={this.btnClickEvent.bind(
                                    this,
                                    item
                                )}
                                {...item}
                            />
                        )
                    })}
                </div>
                <Modal
                    title="接入说明"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="暂不接入" onClick={this.handleCancel}>
                            暂不接入
                        </Button>,
                        <Button
                            key="申请接入"
                            type="primary"
                            onClick={this.handleOk}
                            disabled={this.state.checked ? false : true}
                        >
                            申请接入
                        </Button>
                    ]}
                >
                    {appTrialDesc &&
                        appTrialDesc.split('\n').map((item, index) => {
                            return (
                                <p
                                    key={index}
                                    style={
                                        index === 2 ||
                                        index ===
                                            appTrialDesc.split('\n').length - 1
                                            ? { textIndent: '28px' }
                                            : {}
                                    }
                                >
                                    {item}
                                </p>
                            )
                        })}
                    <Checkbox
                        style={{
                            marginTop: '10px',
                            color: 'rgba(0,126,240,1)'
                        }}
                        onChange={this.handleChangeCkb}
                        checked={this.state.checked ? true : false}
                    >
                        同意
                    </Checkbox>
                </Modal>
                <Modal
                    title="申请延期"
                    visible={this.state.yVisible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancelYq}
                    footer={[
                        <Button key="暂不申请" onClick={this.handleCancelYq}>
                            暂不申请
                        </Button>,
                        <Button
                            key="延期30天"
                            type="primary"
                            onClick={this.handleOkYq}
                            disabled={this.state.checkedYq ? false : true}
                        >
                            延期30天
                        </Button>
                    ]}
                >
                    {appTrialDesc &&
                        appTrialDesc.split('\n').map((item, index) => {
                            return (
                                <p
                                    key={index}
                                    style={
                                        index === 2 ||
                                        index ===
                                            appTrialDesc.split('\n').length - 1
                                            ? { textIndent: '28px' }
                                            : {}
                                    }
                                >
                                    {item}
                                </p>
                            )
                        })}
                    <Checkbox
                        style={{
                            marginTop: '10px',
                            color: 'rgba(0,126,240,1)'
                        }}
                        onChange={this.handleChangeCkbYq}
                        checked={this.state.checkedYq ? true : false}
                    >
                        同意
                    </Checkbox>
                </Modal>
            </div>
        )
    }
}
