import Bimg from 'imgs/banner.jpg'
import * as React from 'react'
import AiList from '../components/aiList/aiList'
import styles from './aiList.less'

export default class PageAiList extends React.Component {
    public render() {
        return (
            <div className={styles.blContent}>
                <div className={styles.bannerBox}>
                    <img src={Bimg} />
                    <a className={styles.btn}>立即前往</a>
                </div>
                <AiList />
            </div>
        )
    }
}
