.
├── README.md
├── build.gradle
├── coverage
│   ├── clover.xml
│   ├── coverage-final.json
│   ├── lcov-report
│   └── lcov.info
├── doc
│   ├── commitlint.md
│   ├── lintstaged.md
│   ├── precommit.md
│   ├── prettier.md
│   ├── stylelint.md
│   ├── tests.md
│   ├── tsconfig.md
│   └── tslint.md
├── docker
│   ├── Dockerfile
│   ├── nginx.conf
│   └── site.conf
├── jest.config.js
├── mock
│   ├── dynamic
│   └── static
├── package.json
├── src
│   ├── app.tsx
│   ├── assets
│   ├── entries
│   ├── modules
│   ├── setUpEnzyme.ts
│   ├── template
│   ├── tests
│   └── type.d.ts
├── tree.md
├── tsconfig.json
├── tslint.json
├── webpack
│   ├── paths.js
│   ├── webpack.common.config.js
│   ├── webpack.dev.config.js
│   └── webpack.prod.config.js
└── yarn.lock

14 directories, 29 files
