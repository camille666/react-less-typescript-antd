import { shallow } from 'enzyme'
import * as React from 'react'
import { CheckboxWithLabel } from '../checkboxWithLabel'

test('CheckboxWithLabel changes the text after click', () => {
    const checkbox = shallow(<CheckboxWithLabel labelOn="On" labelOff="Off" />)

    // 验证默认是off
    expect(checkbox.text()).toEqual('Off')
    // 模拟点击，验证是on
    checkbox.find('input').simulate('change')
    expect(checkbox.text()).toEqual('On')

    expect(shallow).toMatchSnapshot()
})
