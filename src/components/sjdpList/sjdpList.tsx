import { Input } from 'antd'
import axios from 'axios'
import * as React from 'react'
import ListItem from './listItem/listItem'
import styles from './sjdpList.less'
const { Search } = Input

// 从url判断菜单及接口数据返回
const menuId = location.href.charAt(location.href.length - 1)
const isNonjx = location.href.includes('sub') ? 0 : 1

interface IListItemProps {
    icon: string
    appTitle: string
    keywords: string
    appName: string
    overview: string
    appId: string
}

interface ISjdpListState {
    value: string
    appList: IListItemProps[]
}

export default class SjdpList extends React.Component<{}, ISjdpListState> {
    constructor(props: {}) {
        super(props)

        this.state = {
            value: '',
            appList: []
        }
    }

    // 搜索列表
    public handleSearch = (value: string) => {
        this.setState({
            value
        })
    }

    public sendRequest = () => {
        const that = this
        const { value } = this.state
        const reqobj = {
            menuId,
            featured: isNonjx,
            query: value
        }
        axios
            .get('/api/list', { params: reqobj })
            .then(function(response: any) {
                that.setState({
                    appList: response.data.data.apps || []
                })
            })
    }

    public componentDidMount() {
        this.sendRequest()
        window.localStorage.setItem('curpage', '数据大屏')
    }

    public componentDidUpdate(prevProps: any, prevState: any) {
        if (prevState.value !== this.state.value) {
            this.sendRequest()
        }
    }

    public render() {
        return (
            <div className={styles.listBlock}>
                <div className={styles.search}>
                    <span>营销作战室</span>
                    <Search
                        prefix={<span className="iconfont icon-sousuo1" />}
                        placeholder="请输入应用名称"
                        suffix={null}
                        onSearch={this.handleSearch}
                        className={styles.inputS}
                    />
                </div>
                <div className={styles.listBox}>
                    {this.state.appList.map(
                        (item: IListItemProps, index: number) => {
                            return <ListItem key={index} {...item} />
                        }
                    )}
                </div>
            </div>
        )
    }
}
