import React from 'react'

export default class ErrorBoundary extends React.Component {
    public static getDerivedStateFromError() {
        // Update state so the next render will show the fallback UI.
        return { hasError: true }
    }
    public state = {
        hasError: false,
        info: null
    }

    public componentDidCatch(error: Error, info: React.ErrorInfo) {
        this.setState({
            hasError: error,
            errorInfo: info
        })
    }

    public render() {
        if (this.state.hasError) {
            return <h1>请刷新重试</h1>
        }

        return this.props.children
    }
}
