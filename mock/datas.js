module.exports = {
    getList: require('./data/list.js'),
    detailA: require('./data/detailA.js'),
    detailB: require('./data/detailB.js'),
    menu: require('./data/menu.js'),
    rlist: require('./data/rList.js'),
    popWinA: require('./data/popWinA.js'),
    popWinB: require('./data/popWinB.js'),
    tryUse: require('./data/success.js')
}
