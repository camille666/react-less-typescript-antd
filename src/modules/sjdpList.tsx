import Bimg from 'imgs/banner2.jpg'
import * as React from 'react'
import SjdpList from '../components/sjdpList/sjdpList'
import styles from './sjdpList.less'

export default class PageSjdpList extends React.Component {
    public render() {
        return (
            <div className={styles.blContent}>
                <div className={styles.bannerBox}>
                    <img src={Bimg} />
                    <a className={styles.btn}>立即前往</a>
                </div>
                <SjdpList />
            </div>
        )
    }
}
