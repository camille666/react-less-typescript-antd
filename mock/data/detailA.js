module.exports = {
    code: 0,
    data: {
        appId: 'app2',
        appIcon: '',
        level: 1,
        appTitle: 'create',
        appName: '非营销非数据',
        appType: 'battle',
        appUrl: 'http://www.baidu.com',
        brief_intro:
            '致力于服务创意致力于服务创意致力于服务创意致力于服务创意致力于服务创意',
        keywords: '实时创意 历史创意 优质创意',
        overview: '帮助您评估近期广告创意效果',
        appStatus: 1,
        intro: [
            {
                imageUrl:
                    'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565267381686&di=cb366c028cbd9b3e13fb612971886b45&imgtype=0&src=http%3A%2F%2Fb.zol-img.com.cn%2Fsoft%2F6%2F571%2FcepyVKtIjudo6.jpg',
                intro: '这是图片1的介绍，缩略图轮播'
            },
            {
                imageUrl:
                    'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565267459598&di=8750cf0a98119082d5ddf045d9b8f166&imgtype=0&src=http%3A%2F%2Fpic36.nipic.com%2F20131203%2F3822951_102043387000_2.jpg',
                intro: '这是图片2的介绍，缩略图轮播'
            },
            {
                imageUrl:
                    'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565267459597&di=b7169092e8be3ebf3788c7733b1e5e9b&imgtype=0&src=http%3A%2F%2Fpic16.nipic.com%2F20111006%2F6239936_092702973000_2.jpg',
                intro: '这是图片3的介绍，缩略图轮播'
            }
        ],
        userAppStatus: 0
    },
    message: ''
}
