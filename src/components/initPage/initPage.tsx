import * as React from 'react'
import styles from './initPage.less'

export default class InitPage extends React.Component {
    public render() {
        return <div className={styles.loadBox}>页面正在加载</div>
    }
}
