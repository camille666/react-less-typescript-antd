import Bimg from 'imgs/banner1.jpg'
import * as React from 'react'
import AppList from '../components/appList/appList'
import styles from './appList.less'

export default class PageAppList extends React.Component {
    public render() {
        return (
            <div className={styles.blContent}>
                <div className={styles.bannerBox}>
                    <img src={Bimg} />
                    <a className={styles.btn}>立即前往</a>
                </div>
                <AppList />
            </div>
        )
    }
}
