import * as React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import RobotList from '../components/robotList/robotList'
import styles from './robot.less'

interface IRobotState {
    originTitle?: string
}

export default class Robot extends React.Component<
    RouteComponentProps,
    IRobotState
> {
    constructor(props: RouteComponentProps) {
        super(props)
        this.state = {
            originTitle: ''
        }
    }
    public render() {
        return (
            <div className={styles.robotBox}>
                <div className={styles.bread}>
                    <a onClick={() => this.props.history.go(-1)}>
                        {this.state.originTitle}
                    </a>
                    <span
                        className={`iconfont icon-mianbaoxie ${styles.arrowS}`}
                    />
                    <span className={styles.cur}>XXXX数据XXXX</span>
                </div>
                <RobotList />
            </div>
        )
    }
    public componentDidMount() {
        this.setState({
            originTitle: window.localStorage.getItem('curpage')!
        })
    }
}
