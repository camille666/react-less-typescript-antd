import Bimg from 'imgs/banner4.jpg'
import * as React from 'react'
import YxzjList from '../components/yxzjList/yxzjList'
import styles from './yxzjList.less'

export default class PageYxzjList extends React.Component {
    public render() {
        return (
            <div className={styles.blContent}>
                <div className={styles.bannerBox}>
                    <img src={Bimg} />
                    <a className={styles.btn}>立即前往</a>
                </div>
                <YxzjList />
            </div>
        )
    }
}
