import Bimg from 'imgs/banner5.jpg'
import * as React from 'react'
import YyList from '../components/yyList/yyList'
import styles from './yyList.less'

export default class BannerYYList extends React.Component {
    public render() {
        return (
            <div className={styles.blContent}>
                <div className={styles.bannerBox}>
                    <img src={Bimg} />
                    <a className={styles.btn}>立即前往</a>
                </div>
                <YyList />
            </div>
        )
    }
}
