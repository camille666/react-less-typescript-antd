import { Button, Modal } from 'antd'
import axios from 'axios'
import * as React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import TransformBig from '../components/transformBig/transformBig'
import styles from './service.less'

interface IWindowInfo {
    appTrialDesc: string
}

interface IIntroItem {
    imageUrl: string
    intro: string
}

interface IDetail {
    intro: IIntroItem[]
    appTitle: string
    appName: string
    brief_intro: string
    keywords: string
}

interface IYxServiceState {
    lId: string
    visible: boolean
    appDetail: IDetail
    windowInfo: IWindowInfo
}

export default class YxService extends React.Component<
    RouteComponentProps,
    IYxServiceState
> {
    constructor(props: RouteComponentProps) {
        super(props)
        this.state = {
            lId: this.props.history.location.pathname.split('detail2/')[1],
            visible: false,
            appDetail: {
                intro: [],
                appName: '',
                appTitle: '',
                brief_intro: '',
                keywords: ''
            },
            windowInfo: {
                appTrialDesc: ''
            }
        }
    }

    public showModal = () => {
        this.setState({
            visible: true
        })
    }

    public handleOk = (e: any) => {
        this.setState({
            visible: false
        })
    }

    public handleCancel = (e: any) => {
        this.setState({
            visible: false
        })
    }

    // 获取弹窗信息
    public getWindowInfo = () => {
        const that = this
        axios.get(`/api/popWinB`).then(function(response: any) {
            that.setState({
                windowInfo: response.data.data
            })
        })
    }

    // 获取本页面数据
    public getDetail = () => {
        const that = this

        axios.get(`/api/detailB`).then(function(response: any) {
            that.setState({
                appDetail: response.data.data
            })
        })
    }

    public componentDidMount() {
        this.setState({
            lId: this.props.history.location.pathname.split('detail2/')[1]
        })
        this.getDetail()
        this.getWindowInfo()
    }

    public render() {
        const { appName, brief_intro, intro, keywords } = this.state.appDetail
        const { appTrialDesc } = this.state.windowInfo
        return (
            <div className={styles.serviceContent}>
                <div className={styles.bread}>
                    <a onClick={() => this.props.history.go(-1)}>
                        {localStorage.getItem('curpage')}
                    </a>
                    <span
                        className={`iconfont icon-mianbaoxie ${styles.arrowS}`}
                    />
                    <span className={styles.cur}>详情介绍</span>
                </div>
                <div className={styles.detailBlock}>
                    <div className={styles.left}>
                        <img src={''} />
                    </div>
                    <div className={styles.right}>
                        <div className={styles.title}>{appName}</div>
                        <div className={styles.tagBtn}>
                            <div className={styles.tags}>
                                {keywords
                                    .split(' ')
                                    .map((item: string, index: number) => {
                                        return <span key={index}>{item}</span>
                                    })}
                            </div>
                            <a onClick={this.showModal}>立即联系</a>
                        </div>

                        <div className={styles.details}>{brief_intro}</div>
                    </div>
                </div>
                <TransformBig data={intro} />
                <Modal
                    title="提示"
                    visible={this.state.visible}
                    centered={true}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button key="取消" onClick={this.handleCancel}>
                            取消
                        </Button>,
                        <Button
                            key="确认"
                            type="primary"
                            onClick={this.handleOk}
                        >
                            确认
                        </Button>
                    ]}
                >
                    <p style={{ marginBottom: 0, padding: '4px 0' }}>
                        请联系：{appTrialDesc}
                    </p>
                </Modal>
            </div>
        )
    }
}
