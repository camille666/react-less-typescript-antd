import * as React from 'react'

interface ICheckboxWithLabelProps {
    labelOff: string
    labelOn: string
}

interface ICheckboxWithLabelState {
    isChecked: boolean
}

export class CheckboxWithLabel extends React.Component<
    ICheckboxWithLabelProps,
    ICheckboxWithLabelState
> {
    constructor(props: ICheckboxWithLabelProps) {
        super(props)
        this.state = { isChecked: false }
    }

    public onChange = () => {
        this.setState({ isChecked: !this.state.isChecked })
    }

    public render() {
        return (
            <label>
                <input
                    type="checkbox"
                    checked={this.state.isChecked}
                    onChange={this.onChange}
                />
                {this.state.isChecked
                    ? this.props.labelOn
                    : this.props.labelOff}
            </label>
        )
    }
}
