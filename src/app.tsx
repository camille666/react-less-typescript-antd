import ErrorBoundary from 'components/errorBoundary/errorBoundary'
import * as React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import 'styles/base.css'
const HomePage = React.lazy(() => import('modules/index'))
import LoadingPage from 'components/initPage/initPage'
// App
class App extends React.Component {
    public render() {
        return (
            <ErrorBoundary>
                <BrowserRouter>
                    <React.Suspense fallback={<LoadingPage />}>
                        <Switch>
                            <Route
                                path="/"
                                exact={true}
                                render={() => <Redirect to="/appstore/menu1" />}
                            />
                            <Route path="/appstore" component={HomePage} />
                        </Switch>
                    </React.Suspense>
                </BrowserRouter>
            </ErrorBoundary>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root') as HTMLElement)
