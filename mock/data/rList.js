module.exports = {
    code: 0,
    data: [
        {
            iconCode: '',
            mediaDescribe:
                '尊敬的用户：\n现在为您提供为期30天免费的数据接入服务，开通后您可以去往『设置-我的数据源』进行媒体账号的添加。 \n*最终解释权归XXX科技有限公司所有。',
            mediaId: 'media1',
            mediaKeywords: '',
            mediaName: 'XXX',
            mediaOverview: '帮助迅速接入',
            mediaTitle: 'XXX',
            mediaType: 1,
            mediaUrl: 'http://www.baidu.com/setting',
            used: true,
            valid: true
        },
        {
            iconCode: '',
            mediaDescribe:
                '尊敬的用户： \n现在为您提供为期30天免费的数据接入服务，开通后您可以去往『设置-我的数据源』进行媒体账号的添加。 \n*最终解释权归XXX科技有限公司所有。',
            mediaId: 'media2',
            mediaKeywords: '',
            mediaName: 'YYY',
            mediaOverview: '帮助微信接入',
            mediaTitle: 'YYY',
            mediaType: 2,
            mediaUrl: '',
            used: true,
            valid: false
        },
        {
            iconCode: '',
            mediaDescribe:
                '尊敬的用户： \n现在为您提供为期30天免费的数据接入服务，开通后您可以去往『设置-我的数据源』进行媒体账号的添加。 \n*最终解释权归XXX科技有限公司所有。',
            mediaId: 'media3',
            mediaKeywords: '',
            mediaName: 'ZZZ',
            mediaOverview: '帮助腾讯接入',
            mediaTitle: 'ZZZ',
            mediaType: 30,
            mediaUrl: '',
            used: false,
            valid: true
        }
    ],
    message: ''
}
