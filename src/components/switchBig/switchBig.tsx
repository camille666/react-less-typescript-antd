import { Icon } from 'antd'
import * as React from 'react'
import styles from './switchBig.less'

interface ISwitchBigState {
    currentIndex: number
}

interface IIntroItem {
    imageUrl: string
    intro: string
}

interface ISwitchBigProps {
    data: IIntroItem[]
}

// 小图切换大图
export default class SwitchBig extends React.Component<
    ISwitchBigProps,
    ISwitchBigState
> {
    constructor(props: ISwitchBigProps) {
        super(props)
        this.state = {
            currentIndex: 0
        }
    }

    /**
     * 点击左右箭头
     * 1、当前箭头是否激活，改变箭头样式；
     * 2、切换大图
     * 3、改变当前选中的缩略图
     */
    public handlePrevNext = (type: string) => {
        if (type === 'prev') {
            if (!!this.state.currentIndex) {
                this.setState({
                    currentIndex: this.state.currentIndex - 1
                })
            }
        } else if (type === 'next') {
            if (this.state.currentIndex !== this.props.data.length - 1) {
                this.setState({
                    currentIndex: this.state.currentIndex + 1
                })
            }
        }
    }

    // 左右箭头激活？置灰
    public changeBtnStyle = () => {
        const prevBtnStyle =
            this.state.currentIndex === 0 ? styles.disabled : ''
        const nextBtnStyle =
            this.state.currentIndex === this.props.data.length - 1
                ? styles.disabled
                : ''

        return { prevBtnStyle, nextBtnStyle }
    }

    // 点击小图，切换大图
    public handleClickPic = (index: number) => {
        this.setState({
            currentIndex: index
        })
    }

    // 选中缩略图
    public changeSmallStyle = (index: number) => {
        return index === this.state.currentIndex ? styles.enabled : ''
    }

    // 显示大图
    public showBigPic = (index: number) => {
        return index === this.state.currentIndex ? styles.showBig : ''
    }

    // 联动说明文本
    public showCurrentText = () => {
        const filterArr = this.props.data.filter(
            (item: IIntroItem, index: number) =>
                index === this.state.currentIndex
        )[0]
        return filterArr && filterArr.intro
    }

    public render() {
        const imgArr = this.props.data
        return (
            <div className={styles.switchBox}>
                <div className={styles.desc}>
                    <div className={styles.smallTitle}>产品介绍:</div>
                    <p>{this.showCurrentText()}</p>
                </div>

                <div className={styles.imgsBox}>
                    <div className={styles.bigBox}>
                        <a
                            onClick={this.handlePrevNext.bind(this, 'prev')}
                            className={
                                styles.goLeftBtn +
                                ' ' +
                                this.changeBtnStyle().prevBtnStyle
                            }
                        >
                            <Icon
                                type="double-left"
                                style={{ fontSize: '15px' }}
                            />
                        </a>
                        <div className={styles.bigPic}>
                            {imgArr.map((item: IIntroItem, index: number) => {
                                return (
                                    <img
                                        key={index}
                                        src={item.imageUrl}
                                        className={this.showBigPic(index)}
                                    />
                                )
                            })}
                        </div>
                        <a
                            onClick={this.handlePrevNext.bind(this, 'next')}
                            className={
                                styles.goRightBtn +
                                ' ' +
                                this.changeBtnStyle().nextBtnStyle
                            }
                        >
                            <Icon
                                type="double-right"
                                style={{ fontSize: '15px' }}
                            />
                        </a>
                    </div>
                    <div className={styles.smallBox}>
                        {imgArr.map((item: IIntroItem, index: number) => {
                            return (
                                <a
                                    className={this.changeSmallStyle(index)}
                                    onClick={this.handleClickPic.bind(
                                        this,
                                        index
                                    )}
                                    key={index}
                                >
                                    <span className={styles.pointer}>
                                        <span />
                                    </span>
                                    <img src={item.imageUrl} />
                                </a>
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}
