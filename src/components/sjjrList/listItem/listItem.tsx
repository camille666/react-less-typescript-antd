import * as React from 'react'
import { NavLink } from 'react-router-dom'
import styles from './listItem.less'

export interface IListItemProps {
    icon: string
    appTitle: string
    keywords: string
    appName: string
    overview: string
    appId: string
    level: number
}

export default class ListItem extends React.Component<IListItemProps> {
    constructor(props: IListItemProps) {
        super(props)
    }
    public render() {
        const { appName, appId, keywords, overview, icon, level } = this.props
        const kwArr = keywords.split(' ')
        return (
            <NavLink
                className={styles.box}
                to={
                    level === 2
                        ? `/appstore/robot/${appId}`
                        : `/appstore/${location.href.split('appstore/')[1]}`
                }
            >
                <span className={styles.left}>
                    <img src={icon} />
                </span>
                <span className={styles.right}>
                    <span className={styles.title}>{appName}</span>
                    <span className={styles.tags}>
                        {kwArr.map((each: string, index: number) => {
                            return <span key={index}>{each}</span>
                        })}
                    </span>
                    <span className={styles.details}>{overview}</span>
                </span>
            </NavLink>
        )
    }
}
