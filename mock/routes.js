module.exports = {
    '/api/popWinA*': '/popWinA',
    '/api/popWinB*': '/popWinB',
    '/api/detailA*': '/detailA',
    '/api/detailB*': '/detailB',
    '/api/menu*': '/menu',
    '/api/list*': '/getList',
    '/api/rList*': '/rList',
    '/api/tryUse*': '/tryUse',
    '/api/list/:id': '/getList/:id'
}
