import Bimg from 'imgs/banner3.jpg'
import * as React from 'react'
import SjjrList from '../components/sjjrList/sjjrList'
import styles from './sjjrList.less'

export default class PageSjjrList extends React.Component {
    public render() {
        return (
            <div className={styles.blContent}>
                <div className={styles.bannerBox}>
                    <img src={Bimg} />
                    <a className={styles.btn}>立即前往</a>
                </div>
                <SjjrList />
            </div>
        )
    }
}
