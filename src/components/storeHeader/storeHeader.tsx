import { Dropdown, Icon, Menu } from 'antd'
import Logo from 'imgs/home.png'
import * as React from 'react'
import styles from './storeHeader.less'
// const createHistory = require('history').createBrowserHistory

const StoreHeader = () => {
    const logout = async () => {
        // 占位
    }

    const menu = (
        <Menu>
            <Menu.Item>
                <a onClick={logout}>退出登录</a>
            </Menu.Item>
        </Menu>
    )

    return (
        <div className={styles.headerBox}>
            <div className={styles.left}>
                <a
                    onClick={() => {
                        // createHistory.replace(HOME)
                    }}
                >
                    <img src={Logo} />
                </a>
                <span>应用市场</span>
            </div>
            <Dropdown overlay={menu} className={styles.right}>
                <div>
                    {'用户名'}
                    <Icon type="caret-down" />
                </div>
            </Dropdown>
        </div>
    )
}
export default StoreHeader
